import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"; //Login without JWT

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

   //Login without JWT
  constructor(private router:Router) { }

  ngOnInit() {
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
      //this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    }
  }

}
