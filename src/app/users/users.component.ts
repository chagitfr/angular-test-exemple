import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service'; //Q2
import { Router } from "@angular/router"; //Login without JWT

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  //Q2
  users;
  usersKeys = [];

  //Q3
  optimisticAdd(user){
    var newKey = parseInt(this.usersKeys[this.usersKeys.length - 1],0) + 1;
    var newUserObject = {};
    newUserObject['name'] = user; 
    this.users[newKey] = newUserObject;
    this.usersKeys = Object.keys(this.users);
  }

  pessimisticAdd(){
    this.service.getUsers().subscribe(
      response=>{
        this.users = response.json();
        this.usersKeys = Object.keys(this.users);
    })
  }

  //Q4 1
  deleteUser(key){
    console.log.apply(key);
    let index = this.usersKeys.indexOf(key);
    this.usersKeys.splice(index,1);

    //delete from server
    this.service.deleteUser(key).subscribe(
      response=>console.log(response)
    );
  }

  //Q4 2
  updateUser(id){
    this.service.getUser(id).subscribe(response=>{
        this.users = response.json();      
    })
  }

  //Q2
  //Login without JWT router
  constructor(private service:UsersService, private router:Router) {
    service.getUsers().subscribe(
      response=>{
        this.users = response.json();
        this.usersKeys = Object.keys(this.users);
    })
  }

  //Login without JWT
  logout(){ 
      localStorage.removeItem('auth');      
      this.router.navigate(['/login']);
  }
  ngOnInit() {
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
      //this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    }  
  }

}
