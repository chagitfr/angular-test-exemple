import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
//fire base
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';
//Q1
import { RouterModule } from '@angular/router';
//Q2
import { UsersService } from './users/users.service';
//Q3
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AddFormComponent } from './users/add-form/add-form.component';
//Q4
import { UpdateFormComponent } from './users/update-form/update-form.component';
import { UserfireComponent } from './userfire/userfire.component';
//Login without JWT
import { LoginComponent } from './login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    NavigationComponent,
    AddFormComponent,
    UpdateFormComponent,
    UserfireComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    FormsModule,
    ReactiveFormsModule,
     RouterModule.forRoot([
            {path: '', component: UsersComponent},
            {path: 'products', component: ProductsComponent},
            {path: 'update-form/:id', component: UpdateFormComponent},
            {path: 'userfire', component: UserfireComponent},
            {path: 'login', component: LoginComponent},
            {path: '**', component: NotFoundComponent}
          ])
  ],
  providers: [
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
